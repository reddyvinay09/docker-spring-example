package com.docker.dockerspringboot.resource;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/docker")
public class HelloResource {

    @GetMapping
    public String hello(){
        return "Hello welcome to docker world..!";
    }

    @PostMapping
    public String bye(String name){
        return "Nice job Good bye "+name;
    }

}
